import React from 'react';
import ReactDOM from 'react-dom';
import './assets/scss/index.css';
import App from './components/app/App';

ReactDOM.render(
  <App />,
  document.getElementById('root')
);

